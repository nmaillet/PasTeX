A Beamer (LaTeX) template that is really close to the
[official Pasteur Microsoft PowerPoint
template](https://webcampus.pasteur.fr/jcms/c_418955/fr/presentation-institutionnelle-de-l-institut-pasteur-anglais).

![PaSTeX logo](PaSTeX_logo.png)

Configuration
=============

Language
--------

You can set the language using the first line, either english or french:
```latex
\usepackage[english]{languagechooser}
```
or
```latex
\usepackage[french]{languagechooser}
```


Background images for "parts" slides
------------------------------------

You can change default images for each part.
The first one is `img/Part-0.jpg`
Then, each of the 6 parts have the corresponding `img/Part-X.jpg`

Images should be in `1760×1080px` and `100ppi`
Alternatively, you can have `880×540px`.
Always keep ratio of **1.63**.

You should save those images in the `jpeg` format, so as to save space and not end with a
PDF of several hundred of Mb.


Features & limitations
======================

- Use `\maketitle` to have the first slide with title.
- Use `\toc` to have the summary slide.
- Use `\makethanks` to have the last slide with personalized message.
  This message can be more complex than just raw text, i.e.:
  `\thanksmessage{~\\\begin{center}\includegraphics<2>[width=0.32\textwidth]{img/question.png}\end{center}Any question?}`

You can *NOT* have more than 6 parts (this is the same in the official Pasteur
template).


Beamer tips and tricks
======================

- Do not forget the `\pause` Beamer command, to introduce a slide steps by steps.
- You can omit `section`, `subsection` and `frametitle`,
  but be aware that `section` renders the slide title.
- If you want to split your slide in two parts:
```latex
\begin{frame}
\begin{columns}
    \begin{column}{0.5\textwidth}
        LEFT PART
    \end{column}
    \begin{column}{0.5\textwidth}
        RIGHT PART
    \end{column}
\end{columns}
\end{frame}
```
- To render a multi-slide bibliography, allow frame breaks:
```latex
\begin{frame}[allowframebreaks]
    \bibliographystyle{alpha}
    \bibliography{mybiblio.bib}
\end{frame}
```

PasTeX iz in da pl4ce!
